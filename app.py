import logging
from datetime import datetime
from cparse.libs.importer import Importer
from cparse.libs.exporter import Exporter
from utils import Utils
from cparse.libs.config import Config

logger = logging.getLogger(__name__)
appconfig = Config.load()
worksheet = Importer.load_google_worksheet(appconfig['imported_csv'])
remapped_file_name = appconfig['remapped_file_name'] # TODO: Add remapped filename
try:
    data = worksheet.get_all_records()
except AttributeError:
    logger.exception('Either the source file is missing or no records have been found.')
else:
    filename = Utils.dirpath() + remapped_file_name + datetime.now().strftime('%Y%m%d') + '.csv'
    Exporter.write_to_csv(data, filename)
    Utils.transfer_to_sftp()






