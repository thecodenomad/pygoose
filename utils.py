import os
import paramiko
from datetime import datetime
import logging

from cparse.libs.config import Config


class Utils:

    @classmethod
    def dirpath(cls):
        return os.path.dirname(os.path.realpath(__file__))

    @classmethod
    def transfer_to_sftp(cls):
        appconfig = Config.load()
        logger = logging.getLogger(__name__)
        private_key = paramiko.RSAKey.from_private_key_file(
            appconfig['sftp_private_key'])
        filename = 'remapped_' + datetime.now().strftime('%Y%m%d') + '.csv' # NOTE: refactor
        json_data = Config.load()
        LOCAL_PATH = cls.dirpath() + appconfig['local_exports_file_path']
        SFTP_PATH = appconfig['sftp_folder_path'] + filename

        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh_client.connect(hostname=appconfig['sftp_hostname'],
                               username=appconfig['sftp_user'], pkey=private_key)
        except paramiko.ssh_exception.NoValidConnectionsError:
            logger.exception("Could not connect to SFTP server. Check the hostname and try again")
        except paramiko.ssh_exception.SSHException:
            logger.exception("Check the username and pkey and try again")
        else:
            sftp = ssh_client.open_sftp()
            logger.info("Connected to SFTP server.\n\n")

            try:
                sftp.put(LOCAL_PATH, SFTP_PATH)
            except FileNotFoundError:
                logger.exception("Either the local file or filepath or remote (SFTP) filepath are incorrect." + LOCAL_PATH)
            else:
                ssh_client.close()
                logger.info(
                    "The file has been successfully uploaded to the SFTP server.")

