import logging.config
from cparse.libs.config import Config

appconfig = Config.load()

logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '[%(asctime)s] %(levelname)s : %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
        'verbose': {
            'format': '[%(asctime)s] %(levelname)s : [%(name)s.%(funcName)s:%(lineno)d] %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
    },
    'handlers': {
        'console': {
            'level': appconfig['log_level'],
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'file': {
            'level': appconfig['log_level'],
            'class': 'logging.FileHandler',
            'filename': appconfig['log_file_path'], #''/tmp/django_dev.log',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        '': {
            'level': appconfig['log_level'],
            'handlers': ['console', 'file']
        },
        'django.request': {
            'level': appconfig['log_level'],
            'handlers': ['console', 'file']
        }
    }
})