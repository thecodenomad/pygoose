import csv
from cparse.libs.customer import Customer
import logging


class Exporter:
    # Create csv file from imported data
    @classmethod
    def write_to_csv(cls, data, filename):
        logger = logging.getLogger(__name__)

        try:
            csvfile = open(filename, 'w')

            with csvfile:
                writer = csv.writer(csvfile, delimiter=',',
                                    quoting=csv.QUOTE_ALL)
                headings = [] # NOTE: Add Array of Headings here

                writer.writerow(headings)

                customer = Customer()

                for row in data:
                    if row['Timestamp'] == '':
                        data.remove(row)
                    else:
                        customer_row = []

                        # Enter names of columns oin Google Sheets
                        customer_row.append(row['']) # Add first column name
                        customer_row.append(row['']) # Add first column name
                        customer_row.append(row['']) # Add first column name
                        customer_row.append(row['']) # Add first column name
                        customer_row.append(row['']) # Add first column name

                        writer.writerow(customer_row)
        except NameError:
            logger.exception('The filename has not been defined')
        except FileNotFoundError:
            logger.exception('The file to be exported does not exist')
        except KeyError:
            logger.exception(
                'One or more row keys is either missing or incorrect')
        else:
            logger.info(
                "The CSV file has successfully been exported with a total of %s records.", len(data))


