import gspread
from cparse.libs.credentials import Credentials
import logging

class Importer:
    @classmethod
    def authorize_google_sheets(cls):
        credentials = Credentials.load_google_credentials()
        return gspread.authorize(credentials)

    @classmethod
    def load_google_worksheet(cls, filename):
        logger = logging.getLogger(__name__)
        creds = cls.authorize_google_sheets()
        try:
            return creds.open(filename).sheet1
        except gspread.exceptions.SpreadsheetNotFound:
            logger.exception('The Google spreadsheet has not been found.')
        else:
            logger.info('Successfully opened Google spreadsheet.')

