import json

class Config:
    @classmethod
    def load(cls):
        with open('config.json', 'r') as f:
            return json.load(f)