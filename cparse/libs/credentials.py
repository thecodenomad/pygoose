from oauth2client.service_account import ServiceAccountCredentials
from cparse.libs.config import Config
from utils import Utils
import logging
import cparse.libs.logger

class Credentials:
    @classmethod
    def load_google_credentials(cls):
        logger = logging.getLogger(__name__)
        try:
            json_data = Config.load()
        except ModuleNotFoundError:
            logger.exception('Config module is missing.')
        else:
            try:
                scope = json_data['scope_paths'].split(', ')
            except KeyError:
                logger.exception('scope_paths doesn\'t exist in the Config module')
            else:
                try:
                    return ServiceAccountCredentials.from_json_keyfile_name(Utils.dirpath() + '/' + json_data['client_secrets'],
                                                               scope)
                except KeyError:
                    logger.exception('client_secrets doesn\'t exist in the Config module')
                except FileNotFoundError:
                    logger.exception('client_secrets file not found')
                else:
                    logger.info('Google credentials correctly loaded.')