## The application
#### app.py
The application is run from this file by pulling in libraries from the `cparse/libs` folder and from the `utils.py`.
The app executes then following:
- Auth access to Google Drive and Google Sheets
- Import spreadsheet data from Google Drive (Google sheets)
- Format the data and export to new csv file saved in the `exports/` directory (format: `remapped_yyyymmdd.csv`).
- Transfers the exported csv file to the SFTP server determined by the user. 

---
### cparse/libs
#### config.py
##### `config.load():`
Loads the `config.json` file

#### credentials.py
##### `Credentials.load_google_credentials()`
Loads google credentials from the `client _secrets` file (registered in `config.json`)

#### remapper.py
##### `logic_for_remapping`
Determine the logic required for remapping the columns

#### importer.py
##### `Importer.authorize_google_sheets()`
Uses the `Credentials.load()` file to authorize access sheets access

##### `Importer.load_google_worksheet(filename)`
Returns google spreadsheet to be imported

#### exporter.py
##### `customer.product_name(row)`
Exports the reformatted data into a csv file, saved within the `exporter/` folder

#### logger.py
Returns the logging config for the app within the console, as well as in a log file specified in `config.json`. It is also possible to set the log level and log entry format within this file.

---
### / (root path)
#### utils.py

##### `Utils.dirpath()`
Determines the current file path

##### `Utils.transfer_to_sftp(filename)`
Transfers file to SFTP server

---
## Dependencies
All dependencies are listed in `/requirements.txt`. Dependencies include:
  - `oauth2client` for authentication into Google Drive
  - `gspread` for accessing and manipulating Google Sheets
  - `paramiko` for SFTP access
 
---
## IMPORTANT: Please complete the following requirements before running the app
- `config.json.sample` should be copied as `config.json` in the root directory
- add `log_level` (`debug`, `info`, `warning`, etc.), `log_file_path`, `sftp_hostname`, `sftp_user`, `sftp_private_key` to `config.json`.
- add path to log file (e.g. `/var/log/cparse.log`)
- `client_secrets` file placed in the root directory and path added to `config.json`.
- SFTP key file (`******************************.pem`) added to the root directory with the filepath added to `config.json`.
- Rotating log handler should be set within the logging configuration file (`cparse/libs/logger.py`) after the initial period of implementation has been completed.
